1. 修随机博客推荐，详见https://github.com/zh-blogs/blog-daohang/issues/23
2. 支持data-1.json，下载一个json显示加载一个json
3. 相关博客推荐（from和referer）
4. 列表后台和rss订阅
5. 优化体验，访问页面时会卡上几秒
6. 在窄屏打开博客列表，标签会霸占首屏，建议弄成滚动或一个球。
